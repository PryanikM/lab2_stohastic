import numpy as np
from numba import jit
from numba import njit
import matplotlib.pyplot as plt


@jit(nopython=True)
def create_field(field, p, size_x, size_y):
    rnd = np.random.rand(size_x, size_y)
    for row in range(size_x):
        for col in range(size_y):
            if rnd[row, col] < p:
                field[row, col] = 1


@jit(nopython=True)
def dfs(field, x, y, size_x, size_y, offsets):
    for offset_x, offset_y in offsets:
        if 0 <= x + offset_x < size_x and 0 <= y + offset_y < size_y:
            if field[x + offset_x, y + offset_y] == 0 and field[x, y] == 2:
                field[x + offset_x, y + offset_y] = 2
                dfs(field, x + offset_x, y + offset_y, size_x, size_y, offsets)


@jit(nopython=True)
def dfs_without_recirsion(field, x, y, size_x, size_y, offsets):
    indx_start = []
    x_arr = []
    y_arr = []

    indx_start.append(0)
    x_arr.append(x)
    y_arr.append(y)

    i = 0
    while len(indx_start) > 0:
        x = x_arr[i]
        y = y_arr[i]
        is_delete = True
        for indx in range(indx_start[i], len(offsets)):
            offsetsX = offsets[indx][0]
            offsetsY = offsets[indx][1]
            if 0 <= x + offsetsX < size_x and 0 <= y + offsetsY < size_y:
                if field[x + offsetsX, y + offsetsY] == 0 and field[x, y] == 2:
                    field[x + offsetsX, y + offsetsY] = 2

                    indx_start.append(0)
                    x_arr.append(x + offsetsX)
                    y_arr.append(y + offsetsY)
                    x += offsetsX
                    y += offsetsY
                    indx_start[i] = indx + 1
                    i += 1
                    is_delete = False
                    break
        if is_delete:
            del [indx_start[-1]]
            del [x_arr[-1]]
            del [y_arr[-1]]
            i -= 1


@jit(nopython=True)
def percolation(field, size_x, size_y, offsets):
    for col in range(size_y):
        if field[0, col] == 0:
            field[0, col] = 2

    for col in range(size_y):
        if field[0, col] == 2:
            dfs_without_recirsion(field, 0, col, size_x, size_y, offsets)

    if np.any(field[size_x - 1, :] == 2):
        return 1, field
    return 0, field


def start(sizes_x, sizes_y, p_arr, offsets, N_exp, P):
    size_x = 0
    size_y = 0
    w = 0
    W_info = np.zeros((len(sizes_x), N_exp))
    i = 0
    for indx, s_x in enumerate(sizes_x):
        size_x = s_x
        size_y = sizes_y[indx]
        print(size_x, size_y)
        j = 0
        for p in p_arr:
            N_pr = 0
            for experiment in range(N_exp):
                filed = np.zeros(shape=(size_x, size_y))
                create_field(filed, p, size_x, size_y)
                N_pr += percolation(filed, size_x, size_y, offsets)[0]

            w = N_pr / N_exp
            W_info[i, j] = w
            j += 1
        i += 1

    fig, ax = plt.subplots()

    for i in range(len(sizes_x)):
        ax.plot(P, W_info[i, :], label=f'({sizes_x[i]}, {sizes_y[i]})')

    ax.set_xlabel('Вероятность появление диэлектрика')
    ax.set_ylabel('Порого протекания')
    ax.legend()
    # plt.show()

    plt.show()
    return W_info


def create_image(size_x, size_y, p, offsets):
    field = np.zeros(shape=(size_x, size_y))
    create_field(field, p, size_x, size_y)
    start_files = field

    fig, ax = plt.subplots(1, 2)
    ax[0].matshow(start_files)
    result, res = percolation(field, size_x, size_y, offsets)
    ax[1].matshow(res)
    print(f'Протекание: {result}')
    plt.show()


def percolation_threshold(W_info, p_arr, delta, epsilon, N_exp, sizes_x, sizes_y):
    for indx, grid in enumerate(W_info):
        p_delta = delta / 2
        zero_index = np.where(grid == 0)[0][0]
        p_start = p_arr[zero_index]
        p_new = p_start - p_delta
        size_x = sizes_x[indx]
        size_y = sizes_y[indx]
        while abs(p_start - p_new) > epsilon:
            N_pr = 0
            for experiment in range(N_exp):
                filed = np.zeros(shape=(size_x, size_y))
                create_field(filed, p_new, size_x, size_y)
                N_pr = percolation(filed, size_x, size_y, offsets)[0]
                if N_pr == 1:
                    break
            if N_pr == 0:
                break
            else:
                p_delta = p_delta / 2
                p_new = p_start - p_delta

        # print(f'Порог перколяции для сетки ({size_x}, {size_y}) = {"{:.5f}".format(p_new)}')
        print(f'Порог перколяции для сетки ({size_x}, {size_y}) = {p_new}')


#offsets = np.array([[-1, -1], [-1, 0], [-1, 1], [0, -1], [0, 1], [1, -1], [1, 0], [1, 1]])
offsets = np.array([[-1, 0], [0, -1], [0, 1], [1, 0]])
# create_image(16, 16, 0.25, offsets)

N_exp = 100
delta_p = 0.01
epsilon = 1e-6
p_arr = np.arange(0.01, 1.0, delta_p)
sizes = np.arange(4, 8, 1)
sizes_x, sizes_y = 2 ** sizes, 2 ** sizes
P = np.linspace(0., 1., 100)
W_info = start(sizes_x, sizes_y, p_arr, offsets, N_exp, P)
percolation_threshold(W_info, P, delta_p, epsilon, N_exp, sizes_x, sizes_y)

